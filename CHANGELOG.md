SMSGATEWAYHUB 7.x-1.x, 2014-10-10
-------------------------------------
- by subhojit777: Added Scheduled time support for sending SMS

SMSGATEWAYHUB 7.x-1.x, 2014-10-06
-------------------------------------
- by subhojit777: Added Permission to send SMS

SMSGATEWAYHUB 7.x-1.x, 2014-10-06
-------------------------------------
- by subhojit777: Added Credit balance interface

SMSGATEWAYHUB 7.x-1.x, 2014-10-05
-------------------------------------
- by subhojit777: Fixed Password lost if left empty

SMSGATEWAYHUB 7.x-1.x, 2014-10-05
-------------------------------------
- by subhojit777: Added SMS delivery report interface

SMSGatewayHub 7.x-1.x, 2014-09-23
-------------------------------------
- by subhojit777: Changed better watchdog messages
